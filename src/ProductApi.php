<?php
namespace Woompare;

use WC_Product;

/**
 * Modify product api and add some extra fields for compare
 */
class ProductApi
{
    public static function init()
    {
        register_rest_field('product', 'product_attributes', [
            'get_callback' => [self::class, 'getAttributes']
        ]);

        register_rest_field('product', 'product_image', [
            'get_callback' => [self::class, 'getImage']
        ]);

        register_rest_field('product', 'product_price', [
            'get_callback' => [self::class, 'getPrice']
        ]);
    }

    /**
     * Append product feature to request
     *
     * @param array $post
     * @return void
     */
    public static function getAttributes($post)
    {
        $product    = new WC_Product($post['id']);
        $output     = [];
        $attributes = $product->get_attributes();

        $available_attributes = self::getAvailableAttributes();

        foreach ($attributes as $attribute) {
            $taxonomy = $attribute->get_taxonomy_object();
            $id       = $taxonomy->attribute_id;
            $name     = $taxonomy->attribute_name;
            $label    = $taxonomy->attribute_label;

            if (!in_array($id, $available_attributes)) {
                continue;
            }

            $terms    = $attribute->get_terms();
            $options  = [];

            foreach ($terms as $term) {
                $options[$term->term_id] = $term->name;
            }

            $output[$id] = [
                'id'      => $id,
                'name'    => $name,
                'label'   => $label,
                'options' => $options,
            ];
        }

        return $output;
    }

    public static function getImage($post)
    {
        $size = self::getImageSize();
        return get_the_post_thumbnail_url($post['id'], $size);
    }

    public static function getPrice($post)
    {
        $product    = new WC_Product($post['id']);
        return $product->get_display_price();
    }

    public static function getAvailableAttributes()
    {
        $attributes = get_option(Settings::OPTION_ATTRIBUTES_ID);
        return $attributes;
    }

    public static function getImageSize()
    {
        $attributes = get_option(Settings::OPTION_IMAGE_SIZE_ID);
        return $attributes;
    }
}

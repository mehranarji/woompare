<?php
namespace Woompare;

class Settings
{
    public const PAGE_SLUG         = "woompare-settings";
    public const SECTION_MAIN_SLUG = "woompare-main-settings";

    public const OPTION_GROUP_MAIN        = "woompare-main-group";
    public const OPTION_PAGE_TITLE_ID     = "woompare-page-title";
    public const OPTION_TABLE_TITLE_ID    = "woompare-table-title";
    public const OPTION_ATTRIBUTES_ID     = "woompare-attributes";
    public const OPTION_SHOW_ADDTOCART_ID = "woompare-show-addtocart";
    public const OPTION_IMAGE_SIZE_ID     = "woompare-image-size";

    public static function init()
    {
        add_action('admin_menu', [self::class, 'adminPage']);
        add_action('admin_init', [self::class, 'registerSettings']);
    }

    public static function adminPage()
    {
        add_menu_page(
            __('Woompare Settings', 'woompare'),
            __('Woompare', 'woompare'),
            'manage_woocommerce',
            self::PAGE_SLUG,
            [self::class, 'menuCb'],
            'dashicons-feedback',
            30
        );
    }

    public static function registerSettings()
    {
        /**
         * Main settings section
         */
        add_settings_section(
            self::SECTION_MAIN_SLUG,
            __('Woompare Main Settings', 'woompare'),
            [self::class, 'sectionMainCb'],
            self::PAGE_SLUG
        );

        /**
         * Page title field
         */
        add_settings_field(
            self::OPTION_PAGE_TITLE_ID,
            __('Page Title', 'woompare'),
            [FieldGenerator::class, 'text'],
            self::PAGE_SLUG,
            self::SECTION_MAIN_SLUG,
            [
                'label_for' => self::OPTION_PAGE_TITLE_ID,
            ]
        );
        register_setting(self::OPTION_GROUP_MAIN, self::OPTION_PAGE_TITLE_ID,[
            'type' => 'string'
        ]);

        /**
         * Table title field
         */
        add_settings_field(
            self::OPTION_TABLE_TITLE_ID,
            __('Compare Table Title', 'woompare'),
            [FieldGenerator::class, 'text'],
            self::PAGE_SLUG,
            self::SECTION_MAIN_SLUG,
            [
                'label_for' => self::OPTION_TABLE_TITLE_ID,
            ]
        );
        register_setting(self::OPTION_GROUP_MAIN, self::OPTION_TABLE_TITLE_ID,[
            'type' => 'string'
        ]);

        /**
         * Show add to cart field
         */
        add_settings_field(
            self::OPTION_SHOW_ADDTOCART_ID,
            __('Show add to cart button', 'woompare'),
            [FieldGenerator::class, 'checkbox'],
            'woompare-settings',
            'woompare-main-settings',
            [
                'label_for' => self::OPTION_SHOW_ADDTOCART_ID,
            ]
        );
        register_setting(self::OPTION_GROUP_MAIN, self::OPTION_SHOW_ADDTOCART_ID, [
            'type'    => 'boolean',
            'default' => true,
        ]);

        /**
         * Image size field
         */
        add_settings_field(
            self::OPTION_IMAGE_SIZE_ID,
            __('Image Size', 'woompare'),
            [FieldGenerator::class, 'select'],
            'woompare-settings',
            'woompare-main-settings',
            [
                'label_for' => self::OPTION_IMAGE_SIZE_ID,
                'options' => [
                    'thumbnail' => __('Thumbnail', 'woompare'),
                    'medium'    => __('Medium', 'woompare'),
                    'large'     => __('Large', 'woompare')
                ]
            ]
        );
        register_setting(self::OPTION_GROUP_MAIN, self::OPTION_IMAGE_SIZE_ID, [
            'type'    => 'string',
            'default' => 'thumbnail'
        ]);

        /**
         * Product attributes to compare field
         */
        add_settings_field(
            self::OPTION_ATTRIBUTES_ID,
            __('Product attributes to compare', 'woompare'),
            [self::class, 'selectProductAttributesMarkup'],
            'woompare-settings',
            'woompare-main-settings',
            [
                'label_for' => self::OPTION_ATTRIBUTES_ID,
            ]
        );
        register_setting(self::OPTION_GROUP_MAIN, self::OPTION_ATTRIBUTES_ID, [
            'type'    => 'array',
            'default' => []
        ]);
    }

    public static function sectionMainCb()
    {
        _e("You could change main options of woompare here.");
    }

    public static function menuCb()
    {
        ?>
        <div class="wrap">
            <form method="POST" action="options.php">
                <?php
                settings_fields(self::OPTION_GROUP_MAIN);
                do_settings_sections(self::PAGE_SLUG);
                submit_button();
                ?>
            </form>
        </div>
        <?php
    }

    public static function selectProductAttributesMarkup($args)
    {
        $attributes = self::getAllProductAttributes();
        $options    = [];

        foreach ($attributes as $attribute) {
            $options[$attribute->attribute_id] = $attribute->attribute_label;
        }
        $args['options'] = $options;

        FieldGenerator::checkboxArray($args);
    }

    public static function getAllProductAttributes()
    {
        if (function_exists('wc_get_attribute_taxonomies')) {
            $attributes = \wc_get_attribute_taxonomies();
            return $attributes;
        }

        return null;
    }
}

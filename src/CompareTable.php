<?php
namespace Woompare;

class CompareTable
{
    public $products;

    public $title;
    public $show_addtocart;
    public $image_size;
    public $attributes;

    /**
     * @param WC_Product[] $products
     * @param string[] $options
     */
    public function __construct($products, $options = [])
    {
        $this->products = $products;
        
        $this->title          = $options['title'] ?? null;
        $this->image_size     = $options['image-size'] ?? null;
        $this->attributes     = $options['attributes'] ?? null;
        $this->show_addtocart = $options['show-addtocart'] ?? null;
    }

    /**
     * Output table html markup
     *
     * @return void
     */
    public function render()
    {
        ?>
        <form action="" method="GET">
            <div class="woompare-filters">
                <div class="woompare-filter woompare-filter-product">
                    <select name="products[]" class="select2" style="width: 100%"></select>
                </div>
                <div class="woompare-filter woompare-filter-product">
                    <select name="products[]" class="select2" style="width: 100%"></select>
                </div>
            </div>
            
            <div class="woompare-filter woompare-filter-submit">
                <button type="submit" class="woompare-button-submit">مقایسه</button>
            </div>
            
        </form>
        <?php
        
        if (empty($this->products)) {
            return;
        }

        $attributes = $this->getAttributes($this->products);
        ?>
            <div class="woompare-images">
                <?php foreach ($this->products as $product) : ?>
                    <div class="woompare-image-container"><?php echo $product->get_image($this->image_size, ['class' => 'woompare-image']) ?></div>
                <?php endforeach ?>
            </div>

            <table class="woompare-table">
                <tbody>
                    <?php foreach ($attributes as $attribute) : ?>
                    <tr>
                        <th colspan="<?php echo count($this->products) ?>">
                            <?php echo $attribute['label'] ?>
                        </th>
                    </tr>
                    <tr>
                        <?php foreach ($this->products as $product) : ?>
                        <td><?php echo $product->get_attribute($attribute['name']) ?></td>
                        <?php endforeach ?>
                    </tr>
                    <?php endforeach ?>

                    <tr>
                        <?php foreach ($this->products as $product) : ?>
                        <td><?php echo do_shortcode('[add_to_cart id="' . $product->get_id() . '"]') ?></td>
                        <?php endforeach ?>
                    </tr>
                </tbody>
            </table>
        <?php
}

    public function getAttributes($products)
    {
        $attributes = [];

        foreach ($products as $product) {
            $product_attributes = $product->get_attributes();

            foreach ($product_attributes as $attribute) {
                $taxonomy = $attribute->get_taxonomy_object();
                $id       = $taxonomy->attribute_id;
                $name     = $taxonomy->attribute_name;
                $label    = $taxonomy->attribute_label;

                if (!in_array($id, $this->attributes)) {
                    continue;
                }

                if (isset($attributes[$id])) {
                    continue;
                }

                $attributes[$id] = [
                    'id'    => $id,
                    'name'  => $name,
                    'label' => $label,
                ];
            }
        }

        return $attributes;
    }
}

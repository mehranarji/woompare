<?php
namespace Woompare;

use WC_Product;

class Shortcode
{
    public static function init()
    {
        add_shortcode('woompare-table', [self::class, 'showTable']);
        add_shortcode('woompare-table-react', [self::class, 'showTableReact']);
    }

    public static function showTable($atts, $content = '', $tag)
    {
        $options = self::getOptions();
        $products = self::getProducts();
        
        $title = $options['page-title'];

        $table = new CompareTable($products, $options);
        
        // Table out put should be there
        ob_start();
        ?>
            <h2><?php echo $title ?></h2>
            <?php echo $table->render() ?>
        <?php

        return ob_get_clean();
    }

    public static function showTableReact()
    {
        $options = self::getOptions();
        $title = $options['page-title'];
        
        // Table out put should be there
        ob_start();
        ?>
            <h2><?php echo $title ?></h2>
            <div
                id="table-compare"
                title="<?php echo $options['table-title'] ?>"
                show-addtocart="<?php echo $options['show-addtocart'] ?>"
                ></div>
        <?php

        return ob_get_clean();
    }

    public static function getOptions()
    {
        $options = [
            'page-title'     => get_option(Settings::OPTION_PAGE_TITLE_ID, ''),
            'table-title'    => get_option(Settings::OPTION_TABLE_TITLE_ID, ''),
            'show-addtocart' => get_option(Settings::OPTION_SHOW_ADDTOCART_ID, ''),
            'image-size'     => get_option(Settings::OPTION_IMAGE_SIZE_ID, null),
            'attributes'     => get_option(Settings::OPTION_ATTRIBUTES_ID, []),
        ];

        return $options;
    }

    public static function getProducts()
    {
        $product_query = $_GET['products'] ?? null;
        if (!isset($product_query)) {
            return null;
        }

        $product_ids = $_GET['products'];

        $products = [];
        foreach ($product_ids as $id) {
            $products[] = new WC_Product($id);
        }
        return $products;
    }
}

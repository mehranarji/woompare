<?php
namespace Woompare;

class Enqueue
{
    protected static $filename;
    
    public static function init($filename)
    {
        self::$filename = $filename;
        add_action('wp_enqueue_scripts', [self::class, 'enqueue']);
    }

    public static function enqueue()
    {
        wp_enqueue_script(
            'woompare',
            plugin_dir_url(self::$filename) . 'asset/script/dist/app.js',
            [],
            '1.0.0',
            true
        );
        wp_localize_script(
            'app',
            'wp',
            [
                'home_url' => home_url('/')
            ]
        );

        wp_enqueue_style('woompare',
            plugin_dir_url(self::$filename) . 'asset/style/dist/style.css',
            [],
            '1.0.0'
        );
    }
}

<?php
namespace Woompare;

class FieldGenerator
{
    /**
     * Create html class
     *
     * @param string|string[] ...$args
     * @return string
     */
    public static function classNames(...$args) {
        $classnames = [];
        foreach ($args as $arg) {
            if (empty($arg)) {
                continue;
            }

            if (is_array($arg)) {
                $classnames = [...$classnames, ...$arg];
                continue;
            }

            $classnames[] = $arg;
        }

        return join(" ", $classnames);
    }

    /**
     * Output text input html markup
     *
     * @param array $args
     * @return void
     */
    public static function text($args)
    {
        $id    = $args['id'] ?? $args['label_for'];
        $class = $args['class'] ?? null;
        $name  = $args['name'] ?? $args['label_for'];
        $value = get_option($name);
        ?>
            <input
                type="text"
                class="<?php echo self::classNames('regular-text', $class) ?>"
                id="<?php echo $id ?>"
                name="<?php echo $id ?>"
                value="<?php echo $value ?>"
                >
        <?php
    }

    /**
     * Output checkbox html markup
     *
     * @param array $args
     * @return void
     */
    public static function checkbox($args)
    {
        $id       = $args['id'] ?? $args['label_for'];
        $class    = $args['class'] ?? null;
        $name     = $args['name'] ?? $args['label_for'];
        $is_check = get_option($name) === 'true';
        ?>
            <input
                type="checkbox"
                id="<?php echo $id ?>"
                class="<?php echo self::classNames() ?>"
                name="<?php echo $id ?>"
                <?php if ($is_check) : ?>
                    checked=""
                <?php endif ?>
                value="true"
                >
        <?php
    }

    /**
     * Output select html markup
     *
     * @param array $args
     * @return void
     */
    public static function select($args)
    {
        $id      = $args['id'] ?? $args['label_for'];
        $class   = $args['class'] ?? null;
        $name    = $args['name'] ?? $args['label_for'];
        $value   = get_option($name);
        $options = $args['options'] ?? [];
        ?>
            <select
                id="<?php echo $id ?>"
                class="<?php echo self::classNames() ?>"
                name="<?php echo $name ?>"
                >
            <?php foreach ($options as $id => $label) : ?>
                <option
                    value="<?php echo $id ?>"
                    <?php if ($id === $value) : ?>
                    selected=""
                    <?php endif ?>
                    >
                    <?php echo $label ?>
                </option>
            <?php endforeach ?>
            </select>
        <?php
    }

    /**
     * Output select with multiple selectable items html markup
     *
     * @param array $args
     * @return void
     */
    public static function selectMultiple($args)
    {
        $id      = $args['id'] ?? $args['label_for'];
        $class   = $args['class'] ?? null;
        $name    = $args['name'] ?? $args['label_for'];
        $value   = get_option($name);
        $options = $args['options'] ?? [];
        ?>
            <select
                name="<?php echo $name ?>[]"
                id="<?php echo $id ?>"
                class="<?php echo self::classNames() ?>"
                multiple
                >
            <?php foreach ($options as $option_id => $option_label) : ?>
                <option
                    value="<?php echo $option_id ?>"
                    <?php if (in_array($option_id, $value)) : ?>
                    selected=""
                    <?php endif ?>
                    >
                    <?php echo $option_label ?>
                </option>
            <?php endforeach ?>
            </select>
        <?php
    }

    /**
     * Output checkbox array html markup
     *
     * @param array $args
     * @return void
     */
    public static function checkboxArray($args)
    {
        $id      = $args['id'] ?? $args['label_for'];
        $class   = $args['class'] ?? null;
        $name    = $args['name'] ?? $args['label_for'];
        $value   = get_option($name);
        $options = $args['options'] ?? [];
        ?>
            <fieldset>
            <?php foreach ($options as $option_id => $option_label) : ?>
                <label >
                    <input
                        class="<?php echo self::classNames($class) ?>"
                        type="checkbox"
                        name="<?php echo $name ?>[]"
                        value="<?php echo $option_id ?>"
                        <?php if (in_array($option_id, $value)) : ?>
                        checked=""
                        <?php endif ?>
                        >
                    <span><?php echo $option_label ?></span>
                </label>
                <br>
            <?php endforeach ?>
            </fieldset>
        <?php
    }
}

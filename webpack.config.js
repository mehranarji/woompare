const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = (env, argv) => {
    const config = {
        entry: {
            app: './asset/script/src/app.jsx'
        },
        output: {
            path: path.resolve(__dirname),
            filename: 'asset/script/dist/[name].js',
            publicPath: './',

        },
        mode: argv.mode === 'production' ? 'production' : 'development',
        module: {
            rules: [{
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: [{
                    loader: 'babel-loader',
                }]
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    // Compiles Sass to CSS
                    {
                        loader: 'sass-loader',
                        options: {
                            // Prefer `dart-sass`
                            implementation: require('node-sass'),
                            // sourceMap: isDev
                        }
                    }
                ]
            },
            {
                test: /\.(svg)$/i,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 8048,
                        },
                    },
                ],
            },
            {
                test: /\.(ttf|otf|eot|woff2?|png|jpe?g|gif|ico|mp4|webm)$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[path][name].[ext]',
                        esModule: false,
                    }
                }]
            },
            ]
        },
        resolve: {
            extensions: ['*', '.js', '.jsx']
        },
        plugins: [
            new MiniCssExtractPlugin({
                filename: "asset/style/dist/style.css",
                chunkFilename: "[name].css",
                esModule: false
            }),
        ],
        externals: {
            jquery: 'jQuery',
        }
    };

    return config;
};

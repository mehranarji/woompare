import Axios from 'axios'
import { home_url } from "./constant";

const client = Axios.create({
    baseURL: home_url + "wp-json/wp/v2/product"
});

export const Search = ($query) => {
    return client.get("", {
        "params": {
            "search": $query
        }
    });
}

import React, { useEffect, useState } from 'react';
import AsyncSelect from "react-select/async";

import { Search } from "./product-api";
import { home_url } from "./constant";

const count     = 2;
const max_count = 4;

const TableCompare = ({title, showAddToCart}) => {
    const [products, setProducts] = useState(new Array(count).fill(null));

    const getProductsAsOptions = (query, callback) => {
        Search(query)
            .then(({data}) => {
                const options = [];
                data.forEach(item => {
                    options.push({
                        label: item.title.rendered,
                        value: item
                    })
                });

                callback(options);
            })
    };

    const setProductByIndex = (product, index) => {
        let new_products = [...products];
        new_products[index] = product;

        if (new_products.length < max_count && new_products[new_products.length - 1] !== null) {
            new_products.push(null);
        }

        setProducts(new_products);
    };

    const getAttributes = () => {
        let attributes = [];

        for (let index = 0; index < products.length; index++) {
            if (products[index] && products[index].product_attributes) {
                attributes = {...attributes, ...products[index].product_attributes};
            }
        }
        return Object.values(attributes);
    };

    const attributes = getAttributes();

    return <div className="woompare-container">
        <div className="woompare-filters">
            {products.map((item, index) => 
                <div className="woompare-filter woompare-filter-product">
                    <AsyncSelect
                        cacheOptions
                        defaultOptions
                        loadOptions={getProductsAsOptions}
                        onChange={({value}) => setProductByIndex(value, index)}
                        isRtl={true}/>
                </div>
            )}
        </div>

        <div className="woompare-images">
            {products.map((product) => 
                <div className="woompare-image-container">
                { product && product.product_image &&
                    <img src={product.product_image} alt={product.title.rendered} className="woompare-image" />
                }
                </div>
            )}
        </div>

        {attributes.length > 0 ?
            <table className="woompare-table">
                {title && <caption>{title}</caption>}
                <tbody>
                    {attributes.map((attribute) => 
                        <>
                            <tr>
                                <th colSpan={products.length}>{attribute.label}</th>
                            </tr>
                            <tr>
                                {products.map((product) => 
                                    <td>
                                        { product && product.product_attributes && product.product_attributes[attribute.id] &&
                                            Object.values(product.product_attributes[attribute.id].options).join('، ')
                                        }
                                    </td>
                                )}
                            </tr>
                        </>
                    )}
                    {showAddToCart && <tr>
                        {products.map((product) => 
                            <td>
                                { product && 
                                    <a href={`${home_url}?add-to-cart=${product.id}`} className="woompare-addtocart">افزودن به سبد خرید</a>
                                }
                            </td>
                        )}
                    </tr>}
                </tbody>
            </table>
            :
            <div className="woompare-alert woompare-notice">
                <p>لطفا یک محصول را انتخاب کنید</p>
            </div>
        }
    </div>;
}

export default TableCompare;

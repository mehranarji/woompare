import "../../style/src/style.scss";
import React from "react";
import ReactDOM from "react-dom";
import "select2";
import "select2/src/scss/core.scss";

import TableCompare from "./table-compare";
import { home_url } from "./constant";

const TableCompareEl = document.getElementById("table-compare");
if (TableCompareEl) {
    const attributes = {
        title: TableCompareEl.getAttribute('title'),
        showAddToCart: TableCompareEl.getAttribute('show-addtocart')
    };
    ReactDOM.render(<TableCompare {...attributes}/>, TableCompareEl);
}

jQuery('.select2').select2({
    ajax: {
        url: `${home_url}wp-json/wp/v2/product`,
        dir: 'rtl',
        data: (params) => {
            return {
                search: params.term,
                page: params.page
            }
        },
        processResults: (data) => ({
                results: data.map((item) => ({
                id: item.id,
                text: item.title.rendered
            }))
        })
    }
});

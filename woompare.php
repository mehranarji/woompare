<?php
/**
 * Plugin Name:       Woompare
 * Description:       Compare ability for WooCommerce websites
 * Version:           1.0.0
 * Requires at least: 5.0.0
 * Requires PHP:      7.4
 * Author:            Mehran Arjmand
 * Author URI:        https://mehranarji.ir/
 * Text Domain:       woompare
 * 
 * cspell: disable
 */

use Woompare\Enqueue;
use Woompare\ProductApi;
use Woompare\Settings;
use Woompare\Shortcode;

require 'vendor/autoload.php';

Enqueue::init(__FILE__);
Settings::init();
Shortcode::init();
ProductApi::init();
